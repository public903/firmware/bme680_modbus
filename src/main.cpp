/***************************************************************************
  This is a library for the BME680 gas, humidity, temperature & pressure sensor

  Designed specifically to work with the Adafruit BME680 Breakout
  ----> http://www.adafruit.com/products/3660

  These sensors use I2C or SPI to communicate, 2 or 4 pins are required
  to interface.

  Adafruit invests time and resources providing this open source code,
  please support Adafruit and open-source hardware by purchasing products
  from Adafruit!

  Written by Limor Fried & Kevin Townsend for Adafruit Industries.
  BSD license, all text above must be included in any redistribution
 ***************************************************************************/

#include <Arduino.h>
#include <Wire.h>
#include <SPI.h>
#include <Adafruit_Sensor.h>
#include "Adafruit_BME680.h"
#include <avr/wdt.h>
#include <ModbusRTUSlave.h>
#include <SoftwareSerial.h>
#include <EEPROM.h>

#define SEALEVELPRESSURE_HPA (1013.25)

// MODBUS
SoftwareSerial serialMb(6, 5); // RX, TX
ModbusRTUSlave modbus(serialMb);

bool coils[1];
bool discreteInputs[1];
uint16_t holdingRegisters[6]; // 4-ID, 5-Baudrate
uint16_t inputRegisters[1];
uint8_t modbusConfig[2];    // 0-id, 1-baudrate

// BME680
Adafruit_BME680 bme; // I2C
//Adafruit_BME680 bme(BME_CS); // hardware SPI
//Adafruit_BME680 bme(BME_CS, BME_MOSI, BME_MISO,  BME_SCK);

unsigned long previousMillis = 0;   // Premenná pre uloženie predchádzajúceho času
const long interval = 10000;         // Interval v milisekundách (napr. 1000 ms = 1 sekunda)

uint16_t baudRate(uint8_t cislo);
void modbusInit(void);

/*************************************************************************************************************/

void setup() {
  Serial.begin(115200);
  
  wdt_enable(WDTO_8S);
  
  while (!Serial);

  if (!bme.begin()) {
    Serial.println("Could not find a valid BME680 sensor, check wiring!");
    while (1);
  }

  // Set up oversampling and filter initialization
  bme.setTemperatureOversampling(BME680_OS_8X);
  bme.setHumidityOversampling(BME680_OS_2X);
  bme.setPressureOversampling(BME680_OS_4X);
  bme.setIIRFilterSize(BME680_FILTER_SIZE_3);
  bme.setGasHeater(320, 150); // 320*C for 150 ms

  modbus.configureCoils(coils, 2);                       // bool array of coil values, number of coils
  modbus.configureDiscreteInputs(discreteInputs, 2);     // bool array of discrete input values, number of discrete inputs
  modbus.configureHoldingRegisters(holdingRegisters, 6); // unsigned 16 bit integer array of holding register values, number of holding registers
  modbus.configureInputRegisters(inputRegisters, 2);     // unsigned 16 bit integer array of input register values, number of input registers

  modbusInit();
  holdingRegisters[4] = modbusConfig[0];  // Modbus ID
  holdingRegisters[5] = modbusConfig[1];  // Modbus Baudrate
}

/*************************************************************************************************************/

void loop() {
  wdt_reset();
  
  unsigned long currentMillis = millis();
  if (currentMillis - previousMillis >= interval) {   // timer
    previousMillis = currentMillis;

    if (! bme.performReading()) {
      Serial.println("Failed to perform reading :(");
      while(1);
      return;
    }
    holdingRegisters[0] = (uint16_t)(bme.temperature * 10);
    Serial.print("Temperature = holding register 0 = ");
    Serial.print(bme.temperature);
    Serial.println(" *C");

    holdingRegisters[1] = (uint16_t)(bme.pressure / 10);
    Serial.print("Pressure = holding register 1 = ");
    Serial.print(bme.pressure / 100.0);
    Serial.println(" hPa");

    holdingRegisters[2] = (uint16_t)(bme.humidity * 10);
    Serial.print("Humidity = holding register 2 = ");
    Serial.print(bme.humidity);
    Serial.println(" %");

    holdingRegisters[3] = (uint16_t)(bme.gas_resistance / 100);
    Serial.print("Gas = holding register 3 = ");
    Serial.print(bme.gas_resistance / 1000.0);
    Serial.println(" KOhms");

    Serial.print("Approx. Altitude = ");
    Serial.print(bme.readAltitude(SEALEVELPRESSURE_HPA));
    Serial.println(" m");

    Serial.print("Modbus ID = holding register 4 = ");
    Serial.println(modbusConfig[0]);

    Serial.print("Modbus BAUDRATE = holding register 5 = ");
    Serial.print(modbusConfig[1]);
    Serial.print(" = ");
    Serial.println(baudRate(modbusConfig[1]));

    Serial.println();
  }  

  if (holdingRegisters[4] != modbusConfig[0]) {   // zmena modbus ID
    modbusConfig[0] = holdingRegisters[4];
    EEPROM.write(0, modbusConfig[0]);
    modbusInit();
  }
  if (holdingRegisters[5] != modbusConfig[1]) {   // zmena modbus Baudrate
    modbusConfig[1] = holdingRegisters[5];
    EEPROM.write(1, modbusConfig[1]);
    modbusInit();
  }

  modbus.poll();  
}

/*************************************************************************************************************/

uint16_t baudRate(uint8_t cislo) {
  switch(cislo) {
    case 1:
        return(9600);
        break;
    case 2:
        return(19200);
        break;
    case 3:
        return(38400);
        break;
    case 4:
        return(57600);
        break;
    case 5:
        return(115200);
        break;
    default:
        return(9600);
        break;
  }
}

void modbusInit(void) {
  modbusConfig[0] = EEPROM.read(0); // Načítaj z EEPROM adresu Modbus zariadenia
  modbusConfig[1] = EEPROM.read(1); // Načítaj z EEPROM BAUDRATE 1-9600, 2-19200, 3-38400, 4-57600, 5-115200

  if (modbusConfig[0] > 247 || modbusConfig[0] == 0) {
    modbusConfig[0] = 1;
    EEPROM.write(0, modbusConfig[0]);
  }
  if (modbusConfig[1] > 5 || modbusConfig[0] == 0) {
    modbusConfig[1] = 1;
    EEPROM.write(1, modbusConfig[1]);
  }

  modbus.begin(modbusConfig[0], baudRate(modbusConfig[1]));  // adresa, baudrate
}
