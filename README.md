# BME680 Modbus

![BME680_modbus](bme680_modbus.jpg)

**Údaje zo senzora sa dajú načítať z holding registrov (hr) na adrese 0-3:**

- 0 - teplota: hr[0] / 10 = teplota [°C]
- 1 - atmosferický tlak: hr[1] / 10 = tlak [hPa]
- 2 - relatívna vlhkosť vzduchu: hr[2] / 10 = vlhkosť [%]
- 3 - škodlivé plyny v ovzduší: hr[3] / 10 = kvalita vzduchu [kOhm]; hodnota 0-50kOhm je v poriadku pre dýchanie, nad 50 je horšia kvalita vzduchu.

**Modbus konfigurácia:**
- 4 - hr[4] - Modbus ID 1-247
- 5 - hr[5] - Modbus Baudrate 1-5:
    - 1 - 9600
    - 2 - 19200
    - 3 - 38400
    - 4 - 57600
    - 5 - 115200

## Hardware

### BME680

- A5 - SCL
- A4 - SDA

### Modbus
- D5 - Tx
- D6 - Rx